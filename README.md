# arti-example

This is an example project that uses the `arti-client` crate.

See: [This issue](https://gitlab.torproject.org/tpo/core/arti/-/issues/399)
